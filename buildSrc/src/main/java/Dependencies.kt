import Versions.androidCoreVersion
import Versions.appCompatVersion
import Versions.concurrentVersion
import Versions.constraintLayoutVersion
import Versions.espressoVersion
import Versions.fuelVersion
import Versions.glideVersion
import Versions.gsonVersion
import Versions.junitExtVersion
import Versions.junitVersion
import Versions.kotlinVersion
import Versions.listenableFutureCallbackVersion
import Versions.materialVersion
import Versions.preferencesVersion
import Versions.prettytimeVersion
import Versions.realmVersion
import Versions.swipeLayoutVersion
import Versions.workerVersion

object Versions {
    const val kotlinVersion = "1.4.31"
    const val androidCoreVersion = "1.3.2"
    const val appCompatVersion = "1.2.0"
    const val materialVersion = "1.3.0"
    const val constraintLayoutVersion = "2.0.4"
    const val swipeLayoutVersion = "1.1.0"
    const val junitVersion = "4.13.2"
    const val junitExtVersion = "1.1.2"
    const val espressoVersion = "3.3.0"
    const val workerVersion = "2.5.0"

    const val glideVersion = "4.11.0"

    const val fuelVersion = "2.0.1"
    const val gsonVersion = "2.8.6"

    const val realmVersion = "10.5.0"

    const val preferencesVersion = "1.1.1"

    const val prettytimeVersion = "5.0.1.Final"

    const val concurrentVersion = "1.1.0"
    const val listenableFutureCallbackVersion = "1.0.0-beta01"
}

object Dependencies {
    const val kotlinStdlib = "org.jetbrains.kotlin:kotlin-stdlib:$kotlinVersion"
    const val androidCore = "androidx.core:core-ktx:$androidCoreVersion"
    const val appCompat = "androidx.appcompat:appcompat:$appCompatVersion"
    const val material = "com.google.android.material:material:$materialVersion"
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:$constraintLayoutVersion"
    const val swipeLayout = "androidx.swiperefreshlayout:swiperefreshlayout:$swipeLayoutVersion"
    const val junit = "junit:junit:$junitVersion"
    const val junitExt = "androidx.test.ext:junit:$junitExtVersion"
    const val espresso = "androidx.test.espresso:espresso-core:$espressoVersion"
    const val worker = "androidx.work:work-runtime-ktx:$workerVersion"


    const val glide = "com.github.bumptech.glide:glide:$glideVersion"
    const val glideCompiler = "com.github.bumptech.glide:compiler:$glideVersion"

    const val fuel = "com.github.kittinunf.fuel:fuel:$fuelVersion"
    const val fuelAndroid = "com.github.kittinunf.fuel:fuel-android:$fuelVersion"
    const val fuelJson = "com.github.kittinunf.fuel:fuel-json:$fuelVersion"
    const val gson = "com.google.code.gson:gson:$gsonVersion"

    const val realmGradle = "io.realm:realm-gradle-plugin:$realmVersion"

    const val preferences = "androidx.preference:preference-ktx:$preferencesVersion"

    const val prettytime = "org.ocpsoft.prettytime:prettytime:$prettytimeVersion"

    const val concurrent = "androidx.concurrent:concurrent-futures-ktx:$concurrentVersion"
    const val listenableFutureCallback =
        "androidx.concurrent:concurrent-listenablefuture-callback:$listenableFutureCallbackVersion"
}