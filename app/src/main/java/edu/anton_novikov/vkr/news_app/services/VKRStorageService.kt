package edu.anton_novikov.vkr.news_app.services

import edu.anton_novikov.vkr.news_app.enums.VKRNewsTypes
import edu.anton_novikov.vkr.news_app.model.realm.VKRNewsRealm
import edu.anton_novikov.vkr.news_app.model.realm.VKRSourceRealm
import edu.anton_novikov.vkr.news_app.model.view_model.VKRNewsViewModel
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmResults
import org.json.JSONObject

class VKRStorageService {
    companion object {
        private val realm = Realm.getDefaultInstance()

        fun saveNews(data: JSONObject, type: String) {
            var news = data.getJSONArray("articles")

            news = VKRNewsViewModel.putNewsTypeAndDateInJSONArray(news, type)

            realm.executeTransaction {
                if (type != VKRNewsTypes.HEADLINES.type) {
                    it.where(VKRNewsRealm::class.java).equalTo("type", type).findAll()
                        .deleteAllFromRealm()
                }

                realm.createOrUpdateAllFromJson(
                    VKRNewsRealm::class.java,
                    news
                )
            }
        }


        fun loadNews(type: String): RealmResults<VKRNewsRealm> {
            return realm.where(VKRNewsRealm::class.java).equalTo("type", type)
                .findAll()
        }

        fun <T : RealmModel> getObjectFromRealm(
            objectClass: Class<T>,
            fieldValue: String,
            field: String
        ): T? {
            return realm.where(objectClass).equalTo(field, fieldValue).findFirst()
        }

        fun saveSources(data: JSONObject) {
            val sources = data.getJSONArray("sources")

            realm.executeTransaction {
                val sourcesFromRealm = it.where(VKRSourceRealm::class.java).findAll()

                for (i in 0 until sources.length()) {
                    var isFavorite = false

                    for (j in sourcesFromRealm.indices) {
                        if (sourcesFromRealm[j]?.id == (sources[i] as JSONObject)["id"]) {
                            isFavorite = sourcesFromRealm[j]?.isFavorite!!
                        }
                    }


                    (sources[i] as JSONObject).put("isFavorite", isFavorite)
                }
                sourcesFromRealm.deleteAllFromRealm()

                it.createOrUpdateAllFromJson(VKRSourceRealm::class.java, sources)
            }
        }

        fun loadSources(
            category: String,
            language: String,
            country: String
        ): List<VKRSourceRealm> {
            var sources = realm.where(VKRSourceRealm::class.java).findAll().map { item -> item }

            if (category.isNotBlank()) {
                sources = sources.filter { source -> source.category == category }
            }

            if (language.isNotBlank()) {
                sources = sources.filter { source -> source.language == language }
            }

            if (country.isNotBlank()) {
                sources = sources.filter { source -> source.country == country }
            }

            return sources.sortedBy { item -> item.name }
        }

        fun loadFavoriteSources(): RealmResults<VKRSourceRealm> {
            return realm.where(VKRSourceRealm::class.java).equalTo("isFavorite", true).findAll()
        }

        fun changeSourceIsFavorite(sourceId: String) {
            realm.executeTransaction {
                val source =
                    it.where(VKRSourceRealm::class.java).equalTo("id", sourceId).findFirst()
                source?.isFavorite = source?.isFavorite?.not()!!
            }
        }

        fun clearAllFavoriteSources() {
            realm.executeTransaction {
                val sources = loadFavoriteSources()
                for (source in sources) {
                    source.isFavorite = false
                }
            }
        }

        fun deleteAll() {
            realm.executeTransaction {
                it.deleteAll()
            }
        }
    }
}