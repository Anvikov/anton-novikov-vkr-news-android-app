package edu.anton_novikov.vkr.news_app.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.adapters.VKRNewsAdapter
import edu.anton_novikov.vkr.news_app.base.VKRConstant
import edu.anton_novikov.vkr.news_app.enums.VKRNewsTypes
import edu.anton_novikov.vkr.news_app.model.view_model.VKRNewsViewModel
import edu.anton_novikov.vkr.news_app.services.VKRNetworkService
import edu.anton_novikov.vkr.news_app.utils.VKRDisplayUtils
import edu.anton_novikov.vkr.news_app.utils.VKRPagination
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper

class VKRNewsFromFavoriteSourcesFragment(
    private val mContext: Context,
    private val mFragmentManager: FragmentManager
) :
    Fragment(), VKRNetworkService.LoadNewsListener {

    override fun onNewsLoadComplete() {
        news.loadNews(VKRNewsTypes.FROM_FAVORITES.type, "")
    }

    override fun onNewsLoadFailed(error: String) {
        VKRDisplayUtils.showNetworkError(mContext, mFragmentManager, error)
    }


    private lateinit var mView: View
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerViewAdapter: VKRNewsAdapter

    private lateinit var firstPage: ImageButton
    private lateinit var nextPageButton: ImageButton
    private lateinit var previousPageButton: ImageButton
    private lateinit var pageCount: TextView

    private val news: VKRNewsViewModel by viewModels()
    private var pagination = VKRPagination()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.news_from_favorite_sources_fragment, container, false)
        return mView
    }

    override fun onResume() {
        super.onResume()

        initViews()
        initListeners()
        news.getNews(VKRNewsTypes.FROM_FAVORITES.type, "")
            .observe(viewLifecycleOwner) { items ->
                recyclerViewAdapter.setArticles(items, recyclerView)
                showProgressBar(false)
                swipeRefreshLayout.isRefreshing = false
            }
        loadNewsFromFavoriteSources()
    }

    private fun initViews() {
        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh_layout)
        progressBar = mView.findViewById(R.id.newsFromFavoriteSourcesProgressBar)
        recyclerView = mView.findViewById(R.id.recyclerView)

        firstPage = mView.findViewById(R.id.buttonFirstPage)
        nextPageButton = mView.findViewById(R.id.buttonNextPage)
        previousPageButton = mView.findViewById(R.id.buttonPriviosPage)

        pageCount = mView.findViewById(R.id.pageCountView)
        pageCount.text = pagination.page.toString()

        initRecyclerView()
    }

    private fun initRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(mContext)

        recyclerViewAdapter = VKRNewsAdapter(mContext)

        recyclerView.adapter = recyclerViewAdapter
        recyclerViewAdapter.notifyDataSetChanged()
    }

    private fun initListeners() {

        swipeRefreshLayout.setOnRefreshListener {
            pagination.resetPage()
            handleChangePage()
        }

        firstPage.setOnClickListener {
            pagination.resetPage()
            handleChangePage()
        }

        nextPageButton.setOnClickListener {
            pagination.nextPage(if (VKRPreferencesHelper.developerPlan) VKRConstant.developerPlanMaxItemCount else VKRConstant.maxItemsCount)
            handleChangePage()
        }

        previousPageButton.setOnClickListener {
            pagination.previousPage()
            handleChangePage()
        }
    }

    private fun handleChangePage() {
        loadNewsFromFavoriteSources()
        pageCount.text = pagination.page.toString()
    }


    private fun loadNewsFromFavoriteSources() {
        showProgressBar(true)
        VKRNetworkService.loadNews(
            this,
            "",
            VKRFavoriteSourcesFragment.getFavoritesSources(),
            pagination.page, VKRNewsTypes.FROM_FAVORITES.type
        )
    }

    private fun showProgressBar(isNeedShow: Boolean) {
        if (isNeedShow) {
            recyclerView.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }
}