package edu.anton_novikov.vkr.news_app.base

import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper

class VKRConstant {
    companion object {
        const val ENDPOINT_API = "https://newsapi.org/v2"

        var apiKey = VKRPreferencesHelper.apiKey ?: ""
        var developerPlanMaxItemCount = 100
        var maxItemsCount = 999999

        const val notificationChannel = "VKR_Anton_Novikov"
        const val newNewsFromFavoriteSourcesNotificationId = 154
        const val newNewsFromFavoriteSourcesForegroundNotificationId = 155
        const val newNewsFromFavoriteSourcesPendingIntentRequestCode = 157
        const val newsCheckerForegroundNotificationChannel =
            "NewsCheckerForegroundNotificationChannel"
    }
}