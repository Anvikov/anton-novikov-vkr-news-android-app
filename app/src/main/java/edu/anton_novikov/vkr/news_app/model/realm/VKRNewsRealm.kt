package edu.anton_novikov.vkr.news_app.model.realm

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class VKRNewsRealm(
    var source: VKRNewsSourceRealm?,
    var author: String?,
    var title: String?,
    var description: String?,
    @PrimaryKey
    var url: String,
    var urlToImage: String?,
    var publishedAt: Long,
    var content: String?,
    var type: String
) : RealmObject(
) {
    constructor() : this(
        null, "", "", "", "", "", 0, "", ""
    )
}