package edu.anton_novikov.vkr.news_app.ui.fragments

import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.preference.*
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.enums.VKRSortByTypes
import edu.anton_novikov.vkr.news_app.services.VKRFavoritesSourcesNewsChecker
import edu.anton_novikov.vkr.news_app.services.VKRStorageService
import edu.anton_novikov.vkr.news_app.ui.activities.VKRSplashActivity
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper

class VKRSettingFragment : PreferenceFragmentCompat() {
    private var mPreferenceScreen: PreferenceScreen? = null

    private var apiCategory: PreferenceCategory? = null
    private var apiKey: EditTextPreference? = null
    private var developerPlan: SwitchPreference? = null

    private var newsCategory: PreferenceCategory? = null
    private var newsPerPage: SeekBarPreference? = null
    private var newsCacheEnable: SwitchPreference? = null
    private var resetDB: Preference? = null

    private var sortingCategory: PreferenceCategory? = null
    private var sortingEnable: SwitchPreference? = null
    private var sortBy: ListPreference? = null
    private var sortNewsDescending: SwitchPreference? = null

    private var newNewsFromFavoritesSourcesCheckerCategory: PreferenceCategory? = null
    private var newNewsFromFavoritesSourcesChecker: SwitchPreference? = null
    private var newNewsFromFavoritesSourcesCheckerInterval: SeekBarPreference? = null

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        initViews()

        preferenceScreen = mPreferenceScreen

        createApiCategory()
        createNewsCategory()
        createSortingCategory()
        createNewsCheckerCategory()

        setDependencies()
    }

    private fun initViews() {
        mPreferenceScreen = preferenceManager.createPreferenceScreen(context)

        apiCategory = PreferenceCategory(context)
        apiKey = EditTextPreference(context)
        developerPlan = SwitchPreference(context)

        newsCategory = PreferenceCategory(context)
        newsPerPage = SeekBarPreference(context)
        newsCacheEnable = SwitchPreference(context)
        resetDB = Preference(context)

        sortingCategory = PreferenceCategory(context)
        sortingEnable = SwitchPreference(context)
        sortBy = ListPreference(context)
        sortNewsDescending = SwitchPreference(context)

        newNewsFromFavoritesSourcesCheckerCategory = PreferenceCategory(context)
        newNewsFromFavoritesSourcesChecker = SwitchPreference(context)
        newNewsFromFavoritesSourcesCheckerInterval = SeekBarPreference(context)
    }

    private fun createApiCategory() {
        apiCategory?.title = getString(R.string.api)
        apiCategory?.key = "ApiCategory"

        apiKey?.key = "ApiKey"
        apiKey?.title = getString(R.string.set_api_key)
        apiKey?.setDefaultValue(VKRPreferencesHelper.apiKey)
        apiKey?.setOnPreferenceChangeListener { _, newValue ->
            VKRPreferencesHelper.apiKey =
                newValue.toString()
            true
        }

        developerPlan?.key = "DeveloperPlan"
        developerPlan?.title = getString(R.string.developer_plan_preference)
        developerPlan?.summary = getString(R.string.developer_plan_preference_summary)
        developerPlan?.setDefaultValue(VKRPreferencesHelper.developerPlan)
        developerPlan?.setOnPreferenceChangeListener { _, newValue ->
            VKRPreferencesHelper.developerPlan = newValue as Boolean
            true
        }

        mPreferenceScreen?.addPreference(apiCategory)
        apiCategory?.addPreference(apiKey)
        apiCategory?.addPreference(developerPlan)
    }

    private fun createNewsCategory() {
        newsCategory?.key = "NewsCategory"
        newsCategory?.title = getString(R.string.news)

        newsPerPage?.key = "NewsPerPage"
        newsPerPage?.title = getString(R.string.news_per_page)
        newsPerPage?.summary =
            getString(R.string.news_per_page_preference_summary, VKRPreferencesHelper.newsPerPage)
        newsPerPage?.max = 100
        newsPerPage?.min = 5
        newsPerPage?.setDefaultValue(VKRPreferencesHelper.newsPerPage)
        newsPerPage?.showSeekBarValue = true
        newsPerPage?.setOnPreferenceChangeListener { preference, newValue ->
            val value = newValue as Int
            VKRPreferencesHelper.newsPerPage = value
            preference.summary = getString(R.string.news_per_page_preference_summary, value)
            true
        }

        newsCacheEnable?.key = "NewsCacheEnable"
        newsCacheEnable?.title = getString(R.string.news_cache_enable)
        newsCacheEnable?.setDefaultValue(VKRPreferencesHelper.newsCacheEnable)
        newsCacheEnable?.setOnPreferenceChangeListener { _, newValue ->
            val value = newValue as Boolean
            VKRPreferencesHelper.newsCacheEnable = value
            if (!value) context?.cacheDir?.deleteRecursively()
            true
        }

        resetDB?.key = "ResetDB"
        resetDB?.title = getString(R.string.reset_db)
        resetDB?.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            VKRStorageService.deleteAll()
            activity?.startActivity(Intent(context, VKRSplashActivity::class.java))
            activity?.finish()
            true
        }

        mPreferenceScreen?.addPreference(newsCategory)
        newsCategory?.addPreference(newsPerPage)
        newsCategory?.addPreference(newsCacheEnable)
        newsCategory?.addPreference(resetDB)
    }

    private fun createSortingCategory() {
        sortingCategory?.key = "SortingCategory"
        sortingCategory?.title = getString(R.string.sorting)

        sortingEnable?.key = "SortingEnable"
        sortingEnable?.title = getString(R.string.sorting_enable)
        sortingEnable?.setDefaultValue(VKRPreferencesHelper.sortEnable)
        sortingEnable?.setOnPreferenceChangeListener { _, newValue ->
            VKRPreferencesHelper.sortEnable = newValue as Boolean
            true
        }


        val sortByEntries = context?.resources?.getStringArray(R.array.sort_by_preference_entries)
        val sortByTypes = VKRSortByTypes.values()
        var sortByTypeIndex = 0

        sortByTypes.forEachIndexed { index, item ->
            if (item.type == VKRPreferencesHelper.sortBy) sortByTypeIndex = index
        }

        sortBy?.key = "SortBy"
        sortBy?.title = getString(R.string.sort_by)
        sortBy?.summary = getString(R.string.sort_by_summary, sortByEntries?.get(sortByTypeIndex))
        sortBy?.entries = sortByEntries
        sortBy?.entryValues =
            VKRSortByTypes.values().map { item -> item.type }.toTypedArray()
        sortBy?.setDefaultValue(
            sortByTypes[sortByTypeIndex].type
        )
        sortBy?.setOnPreferenceChangeListener { preference, newValue ->
            val value = newValue as String

            sortByTypes.forEachIndexed { index, item ->
                if (item.type == value) sortByTypeIndex = index
            }

            val sortByValue = sortByEntries?.get(sortByTypeIndex)

            VKRPreferencesHelper.sortBy = value
            preference.summary = getString(R.string.sort_by_summary, sortByValue)
            true
        }

        sortNewsDescending?.key = "SortNewsDescending"
        sortNewsDescending?.title = getString(R.string.sort_descending)
        sortNewsDescending?.setDefaultValue(VKRPreferencesHelper.sortByDescending)
        sortNewsDescending?.setOnPreferenceChangeListener { _, newValue ->
            VKRPreferencesHelper.sortByDescending = newValue as Boolean
            true
        }

        mPreferenceScreen?.addPreference(sortingCategory)
        sortingCategory?.addPreference(sortingEnable)
        sortingCategory?.addPreference(sortBy)
        sortingCategory?.addPreference(sortNewsDescending)
    }

    private fun createNewsCheckerCategory() {
        newNewsFromFavoritesSourcesCheckerCategory?.key =
            "NewNewsFromFavoritesSourcesCheckerCategory"
        newNewsFromFavoritesSourcesCheckerCategory?.title =
            getString(R.string.new_news_from_favorites_sources_checker)

        newNewsFromFavoritesSourcesChecker?.key = "NewNewsFromFavoritesSourcesChecker"
        newNewsFromFavoritesSourcesChecker?.title =
            getString(R.string.new_news_from_favorites_sources_checker_title)
        newNewsFromFavoritesSourcesChecker?.setDefaultValue(VKRPreferencesHelper.favoriteSourcesNewsCheckerServiceEnable)
        newNewsFromFavoritesSourcesChecker?.setOnPreferenceChangeListener { _, newValue ->
            val value = newValue as Boolean
            VKRPreferencesHelper.favoriteSourcesNewsCheckerServiceEnable = value


            val serviceIntent = Intent(context, VKRFavoritesSourcesNewsChecker::class.java)
            if (value) {
                context?.let { ContextCompat.startForegroundService(it, serviceIntent) }
            } else context?.stopService(serviceIntent)


            true
        }

        newNewsFromFavoritesSourcesCheckerInterval?.key =
            "NewNewsFromFavoritesSourcesCheckerInterval"
        newNewsFromFavoritesSourcesCheckerInterval?.title =
            getString(R.string.new_news_from_favorites_sources_checker_interval_title)
        newNewsFromFavoritesSourcesCheckerInterval?.summary = getString(
            R.string.new_news_from_favorites_sources_checker_interval_summary,
            VKRPreferencesHelper.favoriteSourcesNewsCheckerServiceInterval
        )
        newNewsFromFavoritesSourcesCheckerInterval?.max = 120
        newNewsFromFavoritesSourcesCheckerInterval?.min = 15
        newNewsFromFavoritesSourcesCheckerInterval?.setDefaultValue(VKRPreferencesHelper.favoriteSourcesNewsCheckerServiceInterval)
        newNewsFromFavoritesSourcesCheckerInterval?.showSeekBarValue = true
        newNewsFromFavoritesSourcesCheckerInterval?.setOnPreferenceChangeListener { preference, newValue ->
            val value = newValue as Int

            VKRPreferencesHelper.favoriteSourcesNewsCheckerServiceInterval = value
            preference.summary =
                getString(R.string.new_news_from_favorites_sources_checker_interval_summary, value)

            true
        }


        mPreferenceScreen?.addPreference(newNewsFromFavoritesSourcesCheckerCategory)
        newNewsFromFavoritesSourcesCheckerCategory?.addPreference(newNewsFromFavoritesSourcesChecker)
        newNewsFromFavoritesSourcesCheckerCategory?.addPreference(
            newNewsFromFavoritesSourcesCheckerInterval
        )
    }

    private fun setDependencies() {
        sortBy?.dependency = "SortingEnable"
        sortNewsDescending?.dependency = "SortingEnable"

        newNewsFromFavoritesSourcesCheckerInterval?.dependency =
            "NewNewsFromFavoritesSourcesChecker"
    }
}