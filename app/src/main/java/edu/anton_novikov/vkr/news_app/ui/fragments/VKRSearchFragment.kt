package edu.anton_novikov.vkr.news_app.ui.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.adapters.VKRNewsAdapter
import edu.anton_novikov.vkr.news_app.base.VKRConstant
import edu.anton_novikov.vkr.news_app.enums.VKRNewsTypes
import edu.anton_novikov.vkr.news_app.model.view_model.VKRNewsViewModel
import edu.anton_novikov.vkr.news_app.services.VKRNetworkService
import edu.anton_novikov.vkr.news_app.utils.VKRDisplayUtils
import edu.anton_novikov.vkr.news_app.utils.VKRPagination
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper

class VKRSearchFragment(
    private val mContext: Context,
    private val mFragmentManager: FragmentManager
) : Fragment(), VKRNetworkService.LoadNewsListener {

    override fun onNewsLoadComplete() {

    }

    override fun onNewsLoadFailed(error: String) {
        VKRDisplayUtils.showNetworkError(mContext, mFragmentManager, error)
    }

    private lateinit var mView: View
    private lateinit var recyclerView: RecyclerView
    private var recyclerViewAdapter = VKRNewsAdapter(mContext)
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var searchView: SearchView
    private var progressBar: ProgressBar? = null

    private var keyword: String = ""
    private val pagination = VKRPagination()
    private val news: VKRNewsViewModel by viewModels()
    private val handler = Handler(Looper.getMainLooper())

    private lateinit var nextPage: ImageButton
    private lateinit var previousPage: ImageButton
    private lateinit var firstPage: ImageButton
    private lateinit var pageCount: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.search_fragment, container, false)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        initListeners()
        news.getNews(VKRNewsTypes.FROM_SEARCH.type, "").observe(
            viewLifecycleOwner,
            { news ->
                recyclerViewAdapter.setArticles(news, recyclerView)
                showRecyclerView(true)
                showProgressBar(false)
                swipeRefreshLayout.isRefreshing = false
            })
    }

    private fun initViews() {
        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh_layout)
        searchView = mView.findViewById(R.id.searchFragmentSearchView)
        recyclerView = mView.findViewById(R.id.recyclerView)
        progressBar = mView.findViewById(R.id.searchFragmentProgressBar)

        nextPage = mView.findViewById(R.id.buttonNextPage)
        previousPage = mView.findViewById(R.id.buttonPriviosPage)
        firstPage = mView.findViewById(R.id.buttonFirstPage)

        pageCount = mView.findViewById(R.id.pageCountView)
        pageCount.text = pagination.page.toString()

        initRecyclerView()
        initHideKeyboardWhenScroll()
        initSearchView()
    }

    private fun initRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(mContext)

        recyclerView.adapter = recyclerViewAdapter
        recyclerViewAdapter.notifyDataSetChanged()
    }

    private fun initListeners() {
        swipeRefreshLayout.setOnRefreshListener {
            if (keyword.isNotBlank()) {
                pagination.resetPage()
                handleChangePage()
            }
        }

        nextPage.setOnClickListener {
            if (keyword.isNotBlank()) {
                pagination.nextPage(if (VKRPreferencesHelper.developerPlan) VKRConstant.developerPlanMaxItemCount else VKRConstant.maxItemsCount)
                handleChangePage()
            }
        }

        previousPage.setOnClickListener {
            if (keyword.isNotBlank()) {
                pagination.previousPage()
                handleChangePage()
            }
        }

        firstPage.setOnClickListener {
            if (keyword.isNotBlank()) {
                pagination.resetPage()
                handleChangePage()
            }
        }

    }

    private fun handleChangePage() {
        pageCount.text = pagination.page.toString()
        showProgressBar(true)
        showRecyclerView(false)
        loadSearchNews()
    }

    private fun showProgressBar(isNeedShow: Boolean) {
        progressBar?.visibility = if (isNeedShow) View.VISIBLE else View.GONE
    }

    private fun showRecyclerView(isNeedShow: Boolean) {
        recyclerView.visibility = if (isNeedShow) View.VISIBLE else View.GONE
    }

    private fun loadSearchNews() {
        VKRNetworkService.loadNews(
            this,
            keyword,
            "",
            pagination.page,
            VKRNewsTypes.FROM_SEARCH.type
        )
    }

    private fun initSearchView() {
        searchView.queryHint = getString(R.string.search_news)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                VKRDisplayUtils.hideKeyboard(searchView)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                keyword = newText ?: ""

                if (newText?.isNotBlank() == true) {
                    handler.removeCallbacksAndMessages(null)
                    handler.postDelayed({ loadSearchNews() }, 1000)
                }
                return false
            }
        })
    }

    private fun initHideKeyboardWhenScroll() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(
                recyclerView: RecyclerView,
                newState: Int
            ) {
                super.onScrollStateChanged(recyclerView, newState)
                VKRDisplayUtils.hideKeyboard(requireView())
            }
        })
    }
}