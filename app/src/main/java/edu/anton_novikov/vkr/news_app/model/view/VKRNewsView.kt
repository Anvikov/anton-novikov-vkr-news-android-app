package edu.anton_novikov.vkr.news_app.model.view

data class VKRNewsView(
    var source: String?,
    var author: String?,
    var title: String?,
    var description: String?,
    var url: String,
    var urlToImage: String?,
    var publishedAt: Long,
    var content: String?,
    var type: String
)