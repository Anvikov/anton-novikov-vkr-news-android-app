package edu.anton_novikov.vkr.news_app.utils

import org.ocpsoft.prettytime.PrettyTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class VKRDateUtils {
    companion object {
        fun convertDateToTimeFormat(oldDate: Long): String {
            val prettyTime = object : PrettyTime(VKRLocaleUtils.getLocale()) {}
            var time = ""

            try {
                time = prettyTime.format(Date(oldDate))
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return time
        }

        fun formatDate(oldDate: Long): String {
            val newDate: String
            val dateFormat = SimpleDateFormat("E, d MMM yyyy", VKRLocaleUtils.getLocale())

            newDate = try {
                dateFormat.format(Date(oldDate))
            } catch (e: ParseException) {
                e.printStackTrace()
                oldDate.toString()
            }

            return newDate
        }

    }
}