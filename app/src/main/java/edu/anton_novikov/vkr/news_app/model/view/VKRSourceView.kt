package edu.anton_novikov.vkr.news_app.model.view

data class VKRSourceView(
    var id: String,
    var name: String?,
    var description: String?,
    var url: String?,
    var category: String?,
    var language: String?,
    var country: String?,
    var isFavorite: Boolean
)