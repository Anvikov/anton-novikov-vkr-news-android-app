package edu.anton_novikov.vkr.news_app.ui.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.adapters.VKRSourcesAdapter
import edu.anton_novikov.vkr.news_app.enums.VKRDialogsTypes
import edu.anton_novikov.vkr.news_app.model.view_model.VKRSourcesViewModel
import edu.anton_novikov.vkr.news_app.utils.VKRDisplayUtils
import edu.anton_novikov.vkr.news_app.utils.VKRLocaleUtils

class VKRSourcesFragment(
    private val mContext: Context
) : Fragment() {

    private var mView: View = View(mContext)
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var recyclerView: RecyclerView
    private var recyclerViewAdapter = VKRSourcesAdapter(listOf(), mContext)
    private lateinit var progressBar: ProgressBar

    private val sources: VKRSourcesViewModel by viewModels()
    private val handler = Handler(Looper.getMainLooper())

    private lateinit var categoryButton: Button
    private lateinit var languageButton: Button
    private lateinit var countryButton: Button
    private var searchView: SearchView? = null

    private var category = ""
    private var language = ""
    private var country = ""
    private var query = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.sources_fragment, container, false)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        initViews()
        initListeners()

        sources.getSources(category, language, country, query)
            .observe(viewLifecycleOwner, { sources ->
                recyclerViewAdapter.setSources(sources)
                swipeRefreshLayout.isRefreshing = false
                Handler(Looper.getMainLooper()).postDelayed({ showProgressBar(false) }, 1000)
            })
    }

    private fun initViews() {
        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh_layout)

        recyclerView = mView.findViewById(R.id.recyclerView)

        categoryButton = mView.findViewById(R.id.buttonCategoryFilterSource)
        languageButton = mView.findViewById(R.id.buttonLanguageFilterSource)
        countryButton = mView.findViewById(R.id.buttonCountryFilterSource)

        progressBar = mView.findViewById(R.id.sourcesProgressBar)

        initRecyclerView()
    }


    private fun initRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(mContext)

        recyclerView.adapter = recyclerViewAdapter
        recyclerViewAdapter.notifyDataSetChanged()
    }

    private fun initListeners() {
        categoryButton.setOnClickListener { createDialog(VKRDialogsTypes.CATEGORY.type).show() }

        languageButton.setOnClickListener { createDialog(VKRDialogsTypes.LANGUAGE.type).show() }

        countryButton.setOnClickListener { createDialog(VKRDialogsTypes.COUNTRY.type).show() }

        swipeRefreshLayout.setOnRefreshListener { loadSources() }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                VKRDisplayUtils.hideKeyboard(requireView())

                if (searchView?.query?.isBlank() == true) {
                    searchView?.isIconified = true
                }
            }
        })
    }

    private fun loadSources() {
        showProgressBar(true)
        sources.loadSources(category, language, country, query)
    }

    private fun showProgressBar(isNeedShow: Boolean) {
        if (isNeedShow) {
            recyclerView.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

    private fun createDialog(type: String): AlertDialog {
        val listItems = when (type) {
            VKRDialogsTypes.CATEGORY.type -> resources.getStringArray(R.array.init_sources_fragment_filter_category_list)
            VKRDialogsTypes.COUNTRY.type -> resources.getStringArray(R.array.sources_fragment_filter_country_list)
            else -> resources.getStringArray(R.array.sources_fragment_filter_language_list)
        }

        val localizedListItems = when (type) {
            VKRDialogsTypes.CATEGORY.type -> VKRLocaleUtils.getFormattedSourceCategories(listItems)
                .toTypedArray()
            VKRDialogsTypes.COUNTRY.type -> VKRLocaleUtils.getCountriesFromCodes(listItems)
                .toTypedArray()
            else -> VKRLocaleUtils.getLanguagesFromCodes(listItems).toTypedArray()
        }

        val item = when (type) {
            VKRDialogsTypes.CATEGORY.type -> category
            VKRDialogsTypes.COUNTRY.type -> country
            else -> language
        }

        val title = when (type) {
            VKRDialogsTypes.CATEGORY.type -> getString(R.string.select_one_category).toLowerCase(
                VKRLocaleUtils.getLocale()
            )
            VKRDialogsTypes.COUNTRY.type -> getString(R.string.select_one_country).toLowerCase(
                VKRLocaleUtils.getLocale()
            )
            else -> getString(R.string.sourcesButtonLanguageFilter).toLowerCase(VKRLocaleUtils.getLocale())
        }

        var selected = if (item.isBlank()) -1 else listItems.indexOf(item)

        val alertDialog = AlertDialog.Builder(requireActivity())
        alertDialog.setSingleChoiceItems(localizedListItems, selected) { _, which ->
            selected = which
        }
        alertDialog.setTitle("${getString(R.string.select_one)} $title")
        alertDialog.setPositiveButton(getString(R.string.select)) { _, _ ->
            when (type) {
                VKRDialogsTypes.CATEGORY.type -> {
                    category = listItems[selected]
                    categoryButton.text = localizedListItems[selected]
                }
                VKRDialogsTypes.COUNTRY.type -> {
                    country = listItems[selected]
                    countryButton.text = localizedListItems[selected]
                }
                else -> {
                    language = listItems[selected]
                    languageButton.text = localizedListItems[selected]
                }
            }

            sources.loadSources(category, language, country, query)
        }
        alertDialog.setNegativeButton(getString(R.string.cancel), null)

        return alertDialog.create()
    }

    private fun handleSearchView() {
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                handler.removeCallbacksAndMessages(null)
                handler.postDelayed({
                    query = newText ?: ""
                    sources.loadSources(
                        category, language, country, query
                    )
                }, 1000)

                return false
            }
        })
        searchView?.maxWidth = Int.MAX_VALUE
        searchView?.queryHint = "${getString(R.string.search_in)} ${getString(R.string.tabSources)}"
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val menuView = inflater.inflate(R.menu.sources_menu, menu)

        val searchMenuItem = menu.findItem(R.id.action_sources_fragment_search)
        searchView = searchMenuItem.actionView as SearchView
        handleSearchView()

        return menuView
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_clear_all_filter_sources -> {
                clearFilterSources()
            }
        }

        return true
    }

    private fun clearFilterSources() {
        category = ""
        language = ""
        country = ""

        categoryButton.text = getString(R.string.sourcesButtonCategoryFilter)
        languageButton.text = getString(R.string.sourcesButtonLanguageFilter)
        countryButton.text = getString(R.string.sourcesButtonCountryFilter)

        loadSources()

        Toast.makeText(mContext, getString(R.string.all_filters_was_cleared), Toast.LENGTH_LONG)
            .show()
    }
}