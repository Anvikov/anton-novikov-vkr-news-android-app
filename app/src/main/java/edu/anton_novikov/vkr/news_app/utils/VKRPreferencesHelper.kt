package edu.anton_novikov.vkr.news_app.utils

import androidx.preference.PreferenceManager
import edu.anton_novikov.vkr.news_app.base.VKRApplication
import edu.anton_novikov.vkr.news_app.enums.VKRSortByTypes


class VKRPreferencesHelper {

    enum class VKRPrefKeys(val preferencesKeyValue: String) {
        API_TOKEN("data.API_TOKEN"),
        DEVELOPER_PLAN("data.DEVELOPER_PLAN"),
        NEWS_PER_PAGE("data.NEWS_PER_PAGE"),
        NEWS_CACHE_ENABLE("data.NEWS_CACHE_ENABLE"),
        SORT_ENABLE("data.SORT_ENABLE"),
        SORT_BY("data.SORT_BY"),
        SORT_BY_DESCENDING("data.SORT_BY_DESCENDING"),
        FAVORITE_SOURCES_NEWS_CHECKER_SERVICE_ENABLE("data.FAVORITE_SOURCES_NEWS_CHECKER_SERVICE_ENABLE"),
        FAVORITE_SOURCES_NEWS_CHECKER_SERVICE_INTERVAL("data.FAVORITE_SOURCES_NEWS_CHECKER_SERVICE_INTERVAL"),
        FAVORITE_SOURCES_NEWS_FIRST_NEWS_URL("data.FAVORITE_SOURCES_NEWS_FIRST_NEWS_URL"),
        FAVORITE_SOURCES_STRING("data.FAVORITE_SOURCES_STRING"),
    }


    companion object {
        private val preferences =
            PreferenceManager.getDefaultSharedPreferences(VKRApplication.getContent())

        var apiKey = preferences.getString(
            VKRPrefKeys.API_TOKEN.preferencesKeyValue,
            "8b9d8d7e10f04ca5848947f474ee028b"
        )
            set(value) {
                preferences.edit()
                    .putString(VKRPrefKeys.API_TOKEN.preferencesKeyValue, value)
                    .apply()

                field = value
            }

        var developerPlan =
            preferences.getBoolean(VKRPrefKeys.DEVELOPER_PLAN.preferencesKeyValue, true)
            set(value) {
                preferences.edit()
                    .putBoolean(VKRPrefKeys.DEVELOPER_PLAN.preferencesKeyValue, value)
                    .apply()

                field = value
            }

        var newsPerPage = preferences.getInt(VKRPrefKeys.NEWS_PER_PAGE.preferencesKeyValue, 20)
            set(value) {
                preferences.edit()
                    .putInt(VKRPrefKeys.NEWS_PER_PAGE.preferencesKeyValue, value)
                    .apply()

                field = value
            }

        var newsCacheEnable =
            preferences.getBoolean(VKRPrefKeys.NEWS_CACHE_ENABLE.preferencesKeyValue, true)
            set(value) {
                preferences.edit()
                    .putBoolean(VKRPrefKeys.NEWS_CACHE_ENABLE.preferencesKeyValue, value)
                    .apply()

                field = value
            }

        var sortEnable =
            preferences.getBoolean(VKRPrefKeys.SORT_ENABLE.preferencesKeyValue, true)
            set(value) {
                preferences.edit()
                    .putBoolean(VKRPrefKeys.SORT_ENABLE.preferencesKeyValue, value)
                    .apply()

                field = value
            }

        var sortBy = preferences.getString(
            VKRPrefKeys.SORT_BY.preferencesKeyValue,
            VKRSortByTypes.PUBLISHED_AT.type
        )
            set(value) {
                preferences.edit()
                    .putString(VKRPrefKeys.SORT_BY.preferencesKeyValue, value)
                    .apply()

                field = value
            }

        var sortByDescending =
            preferences.getBoolean(VKRPrefKeys.SORT_BY_DESCENDING.preferencesKeyValue, false)
            set(value) {
                preferences.edit()
                    .putBoolean(VKRPrefKeys.SORT_BY_DESCENDING.preferencesKeyValue, value)
                    .apply()

                field = value
            }


        var favoriteSourcesNewsCheckerServiceEnable =
            preferences.getBoolean(
                VKRPrefKeys.FAVORITE_SOURCES_NEWS_CHECKER_SERVICE_ENABLE.preferencesKeyValue, true
            )
            set(value) {
                preferences.edit()
                    .putBoolean(
                        VKRPrefKeys.FAVORITE_SOURCES_NEWS_CHECKER_SERVICE_ENABLE.preferencesKeyValue,
                        value
                    )
                    .apply()

                field = value
            }


        var favoriteSourcesNewsCheckerServiceInterval =
            preferences.getInt(
                VKRPrefKeys.FAVORITE_SOURCES_NEWS_CHECKER_SERVICE_INTERVAL.preferencesKeyValue, 30
            )
            set(value) {
                preferences.edit()
                    .putInt(
                        VKRPrefKeys.FAVORITE_SOURCES_NEWS_CHECKER_SERVICE_INTERVAL.preferencesKeyValue,
                        value
                    )
                    .apply()

                field = value
            }

        var favoriteSourcesNewsFirstNewsUrl =
            preferences.getString(
                VKRPrefKeys.FAVORITE_SOURCES_NEWS_FIRST_NEWS_URL.preferencesKeyValue,
                ""
            )
            set(value) {
                preferences.edit()
                    .putString(
                        VKRPrefKeys.FAVORITE_SOURCES_NEWS_FIRST_NEWS_URL.preferencesKeyValue,
                        value
                    )
                    .apply()

                field = value
            }

        var favoriteSourcesString =
            preferences.getString(
                VKRPrefKeys.FAVORITE_SOURCES_STRING.preferencesKeyValue,
                ""
            )
            set(value) {
                preferences.edit()
                    .putString(
                        VKRPrefKeys.FAVORITE_SOURCES_STRING.preferencesKeyValue,
                        value
                    )
                    .apply()

                field = value
            }
    }
}