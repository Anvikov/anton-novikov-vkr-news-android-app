package edu.anton_novikov.vkr.news_app.model.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import edu.anton_novikov.vkr.news_app.enums.VKRNewsTypes
import edu.anton_novikov.vkr.news_app.enums.VKRSortByTypes
import edu.anton_novikov.vkr.news_app.model.realm.VKRNewsRealm
import edu.anton_novikov.vkr.news_app.model.view.VKRNewsView
import edu.anton_novikov.vkr.news_app.services.VKRStorageService
import edu.anton_novikov.vkr.news_app.utils.VKRLocaleUtils
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper
import io.realm.RealmResults
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class VKRNewsViewModel : ViewModel() {
    private val headlinesNews = MutableLiveData<List<VKRNewsView>>()
    private val fromFavoritesSourcesNews = MutableLiveData<List<VKRNewsView>>()
    private val fromSearchingNews = MutableLiveData<List<VKRNewsView>>()
    private val singleNews = MutableLiveData<VKRNewsView>()

    fun getNews(type: String, query: String): LiveData<List<VKRNewsView>> {
        loadNews(type, query)
        return getNewsList(type)
    }

    fun loadNews(type: String, query: String) {
        val realmResult = VKRStorageService.loadNews(type)
        val newsList = getNewsList(type)
        newsList.value = getFilteredNews(realmResult, query)
        realmResult.addChangeListener { news -> newsList.value = getFilteredNews(news, query) }
    }

    fun getSingleNews(url: String): MutableLiveData<VKRNewsView> {
        loadSingleNews(url)
        return singleNews
    }

    private fun loadSingleNews(url: String) {
        val news = VKRStorageService.getObjectFromRealm(VKRNewsRealm::class.java, url, "url")
            ?: VKRNewsRealm()
        singleNews.value = convertNews(news)
    }

    private fun getFilteredNews(
        realmResults: RealmResults<VKRNewsRealm>,
        query: String
    ): List<VKRNewsView> {
        var newsList: List<VKRNewsView> = realmResults.map { item -> convertNews(item) }
        val locale = VKRLocaleUtils.getLocale()

        if (query.isNotEmpty()) {
            newsList = newsList.filter { item ->
                item.title?.toLowerCase(locale)
                    ?.contains(query.toLowerCase(locale)) == true
                        || item.content?.toLowerCase(locale)
                    ?.contains(query.toLowerCase(locale)) == true
            }
        }

        newsList = if (newsList.isNotEmpty()) {
            if (VKRPreferencesHelper.sortEnable)
                return when (VKRPreferencesHelper.sortBy) {
                    VKRSortByTypes.TITLE.type -> if (VKRPreferencesHelper.sortByDescending) newsList.sortedByDescending { item -> item.title } else newsList.sortedBy { item -> item.title }
                    VKRSortByTypes.SOURCE_NAME.type -> if (VKRPreferencesHelper.sortByDescending) newsList.sortedByDescending { item -> item.source } else newsList.sortedBy { item -> item.source }
                    else -> if (VKRPreferencesHelper.sortByDescending) newsList.sortedBy { item -> item.publishedAt } else newsList.sortedByDescending { item -> item.publishedAt }
                }
            else newsList
        } else listOf()

        return newsList
    }

    private fun getNewsList(type: String): MutableLiveData<List<VKRNewsView>> {
        return when (type) {
            VKRNewsTypes.FROM_FAVORITES.type -> fromFavoritesSourcesNews
            VKRNewsTypes.FROM_SEARCH.type -> fromSearchingNews
            else -> headlinesNews
        }
    }

    private fun convertNews(item: VKRNewsRealm): VKRNewsView {
        return VKRNewsView(
            item.source?.name,
            item.author,
            item.title,
            item.description,
            item.url,
            item.urlToImage,
            item.publishedAt,
            item.content,
            item.type
        )
    }

    companion object {
        fun putNewsTypeAndDateInJSONArray(news: JSONArray, type: String): JSONArray {
            for (i in 0 until news.length()) {
                val article = news.get(i) as JSONObject
                val publishedAt = article.get("publishedAt")

                article.remove("publishedAt")
                article.put("type", type)
                article.put("publishedAt", shiftNewsTime(publishedAt.toString()))
            }

            return news
        }

        private fun shiftNewsTime(publishedAt: String): Long {
            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH)
            sdf.timeZone = TimeZone.getTimeZone("GMT+0")

            val date = sdf.parse(publishedAt) ?: Date()
            val calendar = GregorianCalendar()
            calendar.time = date
            return calendar.time.time
        }
    }
}

