package edu.anton_novikov.vkr.news_app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.model.view.VKRNewsView
import edu.anton_novikov.vkr.news_app.services.VKRNetworkService
import edu.anton_novikov.vkr.news_app.ui.activities.VKRMainActivity
import edu.anton_novikov.vkr.news_app.utils.VKRDateUtils

class VKRNewsAdapter(
    private val mContext: Context
) :
    RecyclerView.Adapter<VKRNewsAdapter.MyViewHolder>() {
    private var newsRealms: List<VKRNewsView> = listOf()
    private val listener = VKRMainActivity.getOnNewsItemClickedListener()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mContext).inflate(R.layout.news_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return newsRealms.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val news = newsRealms[position]

        VKRNetworkService.loadImageAndHideProgressBar(
            news.urlToImage ?: "",
            holder.imageView,
            holder.progressBar
        )
        fillNewsDetail(holder, news)

        holder.itemView.setOnClickListener {
            listener.onNewsItemClick(news.url)
        }
    }

    fun setArticles(data: List<VKRNewsView>, recyclerView: RecyclerView) {
        recyclerView.smoothScrollToPosition(0)
        newsRealms = data
        notifyDataSetChanged()
    }


    private fun fillNewsDetail(holder: MyViewHolder, model: VKRNewsView) {
        holder.title.text = model.title
        holder.description.text = model.description
        holder.source.text = model.source
        holder.time.text = mContext.getString(
            R.string.time_with_delimiter,
            VKRDateUtils.convertDateToTimeFormat(model.publishedAt)
        )
        holder.publishedAt.text = VKRDateUtils.formatDate(model.publishedAt)
        holder.author.text = model.author
    }

    interface OnNewsItemClickListener {
        fun onNewsItemClick(url: String) {}
    }

    class MyViewHolder(
        view: View,
    ) : RecyclerView.ViewHolder(view) {
        var title: TextView = itemView.findViewById(R.id.name)
        var description: TextView = itemView.findViewById(R.id.description)
        var author: TextView = itemView.findViewById(R.id.author)
        var publishedAt: TextView = itemView.findViewById(R.id.publishedAt)
        var source: TextView = itemView.findViewById(R.id.source)
        var time: TextView = itemView.findViewById(R.id.time)

        var imageView: ImageView = itemView.findViewById(R.id.img)
        var progressBar: ProgressBar = itemView.findViewById(R.id.progress_loading_photo)
    }
}