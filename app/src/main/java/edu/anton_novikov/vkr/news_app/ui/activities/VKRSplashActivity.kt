package edu.anton_novikov.vkr.news_app.ui.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.services.VKRNetworkService

class VKRSplashActivity : AppCompatActivity(), VKRNetworkService.LoadHeadlinesListener,
    VKRNetworkService.LoadSourcesListener {
    override fun onHeadlinesNewsLoadComplete() {
        loadAllSources()
    }

    override fun onHeadlinesNewsLoadFailed(error: String) {
        if (!isRetryLoadHeadlinesNews) {
            VKRNetworkService.loadHeadlines(this)
            isRetryLoadHeadlinesNews = true
        } else {
            loadAllSources()
        }
    }

    override fun onSourcesLoadComplete() {
        startMainActivity()
    }

    override fun onSourcesLoadFailed(error: String) {
        if (!isRetryLoadSources) {
            loadAllSources()
            isRetryLoadSources = true
        } else {
            startMainActivity()
        }
    }

    private var isRetryLoadHeadlinesNews = false
    private var isRetryLoadSources = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity_layout)
        supportActionBar?.hide()

        if (VKRNetworkService.isInternetAvailable(this)) {
            VKRNetworkService.loadHeadlines(this)
        } else {
            startMainActivity()
        }
    }

    private fun startMainActivity() {
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this, VKRMainActivity::class.java))
            finish()
        }, 1000)
    }

    private fun loadAllSources() {
        VKRNetworkService.loadSources(this, "", "", "")
    }
}