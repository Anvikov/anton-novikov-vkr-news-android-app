package edu.anton_novikov.vkr.news_app.ui.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.adapters.VKRNewsAdapter
import edu.anton_novikov.vkr.news_app.enums.VKRNewsTypes
import edu.anton_novikov.vkr.news_app.model.view.VKRNewsView
import edu.anton_novikov.vkr.news_app.model.view_model.VKRNewsViewModel
import edu.anton_novikov.vkr.news_app.services.VKRNetworkService
import edu.anton_novikov.vkr.news_app.utils.VKRDisplayUtils
import edu.anton_novikov.vkr.news_app.utils.VKRPagination

class VKRTopHeadlines(
    private val mContext: Context,
    private val mFragmentManager: FragmentManager
) : Fragment(), VKRNetworkService.LoadHeadlinesListener {

    override fun onHeadlinesNewsLoadComplete() {
        newsViewModel.loadNews(VKRNewsTypes.HEADLINES.type, "")
    }

    override fun onHeadlinesNewsLoadFailed(error: String) {
        swipeRefreshLayout.isRefreshing = false
        VKRDisplayUtils.showNetworkError(mContext, mFragmentManager, error)
    }

    private val handler = Handler(Looper.getMainLooper())
    private val newsViewModel: VKRNewsViewModel by viewModels()
    private var news = listOf<VKRNewsView>()
    private val pagination = VKRPagination()

    private lateinit var mView: View
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var progressBar: ProgressBar

    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerViewAdapter: VKRNewsAdapter
    private val recyclerViewLayoutManager = LinearLayoutManager(mContext)

    private lateinit var nextPage: ImageButton
    private lateinit var lastPage: ImageButton
    private lateinit var lastPageLayout: ConstraintLayout
    private lateinit var previousPage: ImageButton
    private lateinit var firstPage: ImageButton
    private lateinit var pageCount: TextView

    private var searchView: SearchView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.top_headlines, container, false)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)
        initViews()
        initListeners()
        newsViewModel.getNews(VKRNewsTypes.HEADLINES.type, "")
            .observe(viewLifecycleOwner, { newsList ->
                news = newsList
                pagination.resetPage()
                changeNewsList()
                swipeRefreshLayout.isRefreshing = false
            })
    }

    private fun initViews() {
        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh_layout)
        progressBar = mView.findViewById(R.id.topHeadlinesProgressBar)
        recyclerView = mView.findViewById(R.id.recyclerView)

        nextPage = mView.findViewById(R.id.buttonNextPage)
        lastPage = mView.findViewById(R.id.buttonLastPage)
        lastPageLayout = mView.findViewById(R.id.buttonLastPageLayout)
        lastPageLayout.visibility = View.VISIBLE
        previousPage = mView.findViewById(R.id.buttonPriviosPage)
        firstPage = mView.findViewById(R.id.buttonFirstPage)
        pageCount = mView.findViewById(R.id.pageCountView)

        initRecyclerView()
    }

    private fun initRecyclerView() {
        recyclerView.layoutManager =
            recyclerViewLayoutManager

        recyclerViewAdapter = VKRNewsAdapter(mContext)
        recyclerView.adapter = recyclerViewAdapter
        recyclerViewAdapter.notifyDataSetChanged()
    }

    private fun initListeners() {
        swipeRefreshLayout.setOnRefreshListener {
            if (VKRNetworkService.isInternetAvailable(mContext)) {
                VKRNetworkService.loadHeadlines(this)
            } else swipeRefreshLayout.isRefreshing = false
        }

        nextPage.setOnClickListener {
            pagination.nextPage(news.size)
            changeNewsList()
        }

        lastPage.setOnClickListener {
            pagination.lastPage(news.size)
            changeNewsList()
        }

        previousPage.setOnClickListener {
            pagination.previousPage()
            changeNewsList()
        }

        firstPage.setOnClickListener {
            pagination.resetPage()
            changeNewsList()
        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                VKRDisplayUtils.hideKeyboard(requireView())

                if (searchView?.query?.isBlank() == true) {
                    searchView?.isIconified = true
                }
            }
        })
    }

    private fun changeNewsList() {
        showProgressBar(true)
        handler.postDelayed({
            val index = pagination.getNewsListIndex(news.size)
            recyclerViewAdapter.setArticles(news.subList(index[0], index[1]), recyclerView)
            pageCount.text = pagination.page.toString()
            showProgressBar(false)
        }, 250)
    }

    private fun showProgressBar(isNeedShow: Boolean) {
        if (isNeedShow) {
            recyclerView.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

    private fun handleSearchView() {
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                handler.removeCallbacksAndMessages(null)
                handler.postDelayed({
                    newsViewModel.loadNews(
                        VKRNewsTypes.HEADLINES.type,
                        newText ?: ""
                    )
                }, 1000)

                return false
            }
        })

        searchView?.maxWidth = Int.MAX_VALUE
        searchView?.queryHint =
            "${getString(R.string.search_in)} ${getString(R.string.tabHeadlines)}"
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val menuView = inflater.inflate(R.menu.top_headlines_menu, menu)

        val searchMenuItem = menu.findItem(R.id.action_top_headlines_search)
        searchView = searchMenuItem.actionView as SearchView
        handleSearchView()

        return menuView
    }
}