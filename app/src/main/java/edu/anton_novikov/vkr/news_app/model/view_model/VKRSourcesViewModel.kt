package edu.anton_novikov.vkr.news_app.model.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import edu.anton_novikov.vkr.news_app.model.realm.VKRSourceRealm
import edu.anton_novikov.vkr.news_app.model.view.VKRSourceView
import edu.anton_novikov.vkr.news_app.services.VKRStorageService
import edu.anton_novikov.vkr.news_app.utils.VKRLocaleUtils

class VKRSourcesViewModel : ViewModel() {
    private val sources = MutableLiveData<List<VKRSourceView>>()
    private val favoriteSources = MutableLiveData<List<VKRSourceView>>()

    fun getSources(
        category: String,
        language: String,
        country: String, query: String
    ): LiveData<List<VKRSourceView>> {
        loadSources(category, language, country, query)
        return sources
    }

    fun getFavoriteSources(
    ): LiveData<List<VKRSourceView>> {
        loadFavoriteSources()
        return favoriteSources
    }

    fun loadSources(category: String, language: String, country: String, query: String) {
        val locale = VKRLocaleUtils.getLocale()
        sources.value = VKRStorageService.loadSources(category, language, country).filter { item ->
            item.name?.toLowerCase(locale)
                ?.contains(query.toLowerCase(locale)) == true
        }.map { item ->
            convertSource(item)
        }
    }

    private fun loadFavoriteSources() {
        val realmResults = VKRStorageService.loadFavoriteSources()

        favoriteSources.value =
            realmResults.sort("name").map { item -> convertSource(item) }

        realmResults.addChangeListener { items ->
            favoriteSources.value = items.sort("name").map { item -> convertSource(item) }
        }
    }

    private fun convertSource(item: VKRSourceRealm): VKRSourceView {
        return VKRSourceView(
            item.id,
            item.name,
            item.description,
            item.url,
            item.category,
            item.language,
            item.country,
            item.isFavorite
        )
    }
}