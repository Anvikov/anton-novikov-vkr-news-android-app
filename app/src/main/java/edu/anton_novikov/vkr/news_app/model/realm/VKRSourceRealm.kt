package edu.anton_novikov.vkr.news_app.model.realm

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class VKRSourceRealm(
    @PrimaryKey
    var id: String,
    var name: String?,
    var description: String?,
    var url: String?,
    var category: String?,
    var language: String?,
    var country: String?,
    var isFavorite: Boolean
) : RealmObject() {
    constructor() : this("", "", "", "", "", "", "", false)
}