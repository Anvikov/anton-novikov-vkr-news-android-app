package edu.anton_novikov.vkr.news_app.ui.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.bottomnavigation.BottomNavigationView
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.adapters.VKRNewsAdapter
import edu.anton_novikov.vkr.news_app.services.VKRFavoritesSourcesNewsChecker
import edu.anton_novikov.vkr.news_app.ui.fragments.*
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper


class VKRMainActivity : AppCompatActivity(), VKRNewsAdapter.OnNewsItemClickListener {

    init {
        instance = this
    }

    override fun onNewsItemClick(url: String) {
        val intent = Intent(this, VKRNewsDetailActivity::class.java)
        intent.putExtra(this.getString(R.string.url), url)
        startActivity(intent)
    }


    private var bottomNavigationView: BottomNavigationView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        changeFragment(R.id.action_top_headlines)
        createBottomNavigationView()
        startNewsChecker()
        checkIntent()
    }

    private fun createBottomNavigationView() {
        bottomNavigationView = findViewById(R.id.bottom_navigation)

        bottomNavigationView!!.setOnNavigationItemSelectedListener { item ->
            changeFragment(item.itemId)
            true
        }
    }

    private fun changeFragment(id: Int) {
        val containerId = R.id.fragmentLayout
        val fragment = when (id) {
            R.id.action_sources ->
                VKRSourcesFragment(baseContext)

            R.id.action_favorites ->
                VKRFavoriteSourcesFragment(baseContext, supportFragmentManager)

            R.id.action_search ->
                VKRSearchFragment(baseContext, supportFragmentManager)

            R.id.action_settings -> VKRSettingFragment()

            else -> VKRTopHeadlines(baseContext, supportFragmentManager)
        }

        supportFragmentManager.beginTransaction().replace(containerId, fragment)
            .commitAllowingStateLoss()

        changeToolbarTitle(id)
    }

    private fun changeToolbarTitle(id: Int) {
        supportActionBar?.title = when (id) {
            R.id.action_sources -> getString(R.string.tabSources)

            R.id.action_favorites -> getString(R.string.tabFavoriteSources)

            R.id.action_search -> getString(R.string.tabSearch)

            R.id.action_settings -> getString(R.string.tabSettings)

            else -> getString(R.string.tabHeadlines)
        }
    }

    private fun checkIntent() {
        val extra =
            intent.getBooleanExtra(getString(R.string.open_news_from_favorites_sources), false)

        if (extra) {
            startActivity(Intent(this, VKRNewsFromFavoriteSourcesActivity::class.java))
        }
    }

    private fun startNewsChecker() {
        val intent = Intent(this, VKRFavoritesSourcesNewsChecker::class.java)

        if (VKRPreferencesHelper.favoriteSourcesNewsCheckerServiceEnable) {
            ContextCompat.startForegroundService(this, intent)
        } else stopService(intent)
    }

    companion object {
        private var instance: VKRMainActivity? = null

        fun getOnNewsItemClickedListener(): VKRNewsAdapter.OnNewsItemClickListener {
            return instance!!
        }

        fun openUrl(url: String) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            instance?.startActivity(intent)
        }
    }
}