package edu.anton_novikov.vkr.news_app.enums

enum class VKRSortByTypes(val type: String) {
    TITLE("title"),
    SOURCE_NAME("sourceName"),
    PUBLISHED_AT("publishedAt")
}