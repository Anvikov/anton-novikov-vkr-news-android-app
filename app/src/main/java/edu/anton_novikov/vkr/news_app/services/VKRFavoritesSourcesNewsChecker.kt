package edu.anton_novikov.vkr.news_app.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.api.VKRAPIService
import edu.anton_novikov.vkr.news_app.base.VKRApplication
import edu.anton_novikov.vkr.news_app.base.VKRConstant
import edu.anton_novikov.vkr.news_app.ui.activities.VKRMainActivity
import edu.anton_novikov.vkr.news_app.utils.VKRDisplayUtils
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper
import org.json.JSONObject

class VKRFavoritesSourcesNewsChecker : Service() {
    private val notificationManager by lazy { applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager }
    private val handler by lazy { Handler(Looper.getMainLooper()) }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(
                VKRConstant.newNewsFromFavoriteSourcesForegroundNotificationId,
                getForegroundNotification()
            )
        }

        startCheckNews()

        return START_STICKY
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        sendBroadcast(Intent(applicationContext, VKRNewsCheckerBroadcastReceiver::class.java))
        super.onTaskRemoved(rootIntent)
    }

    private fun startCheckNews() {
        if (VKRPreferencesHelper.favoriteSourcesNewsCheckerServiceEnable) {
            checkNewNews()

            handler.postDelayed(
                { startCheckNews() },
                VKRPreferencesHelper.favoriteSourcesNewsCheckerServiceInterval * 60 * 1000L
            )
        } else {
            stopSelf()
        }
    }

    private fun checkNewNews() {
        val favoritesSources = VKRPreferencesHelper.favoriteSourcesString

        if (favoritesSources?.isNotBlank() == true) {
            VKRAPIService.getNews("", favoritesSources, 1)
                .responseJson { _, _, result ->
                    if (result is Result.Success) {
                        val json = result.get().obj()
                        val firstNewsUrl =
                            (json.getJSONArray("articles")[0] as JSONObject).getString("url")

                        if (firstNewsUrl != VKRPreferencesHelper.favoriteSourcesNewsFirstNewsUrl) {
                            VKRPreferencesHelper.favoriteSourcesNewsFirstNewsUrl = firstNewsUrl
                            showPushNotification()
                        }
                    }
                }
        }
    }

    private fun getNotification(): Notification {
        val notificationBuilder =
            NotificationCompat.Builder(applicationContext, VKRConstant.notificationChannel)

        val resultIntent =
            Intent(VKRApplication.getContent(), VKRMainActivity::class.java)
        resultIntent.putExtra(
            applicationContext.getString(R.string.open_news_from_favorites_sources),
            true
        )

        val resultPendingIntent =
            PendingIntent.getActivity(
                VKRApplication.getContent(),
                VKRConstant.newNewsFromFavoriteSourcesPendingIntentRequestCode,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )


        notificationBuilder.setSmallIcon(R.drawable.notification_icon)
        notificationBuilder.setContentTitle(applicationContext.getString(R.string.notification_title))
        notificationBuilder.setContentText(applicationContext.getString(R.string.notification_text))
        notificationBuilder.setContentIntent(resultPendingIntent)
        notificationBuilder.setAutoCancel(true)

        return notificationBuilder.build()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getForegroundNotification(): Notification {
        val notificationChannel = NotificationChannel(
            VKRConstant.newsCheckerForegroundNotificationChannel,
            getString(R.string.news_checker_foreground_notification_channel_name),
            NotificationManager.IMPORTANCE_LOW
        )

        notificationManager.createNotificationChannel(notificationChannel)

        return Notification.Builder(applicationContext, VKRConstant.notificationChannel)
            .setContentTitle(getString(R.string.waiting_for_new_news))
            .setSmallIcon(R.drawable.notification_icon)
            .setChannelId(VKRConstant.newsCheckerForegroundNotificationChannel).build()
    }

    private fun showPushNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                VKRConstant.notificationChannel,
                VKRDisplayUtils.capitalize(applicationContext.getString(R.string.sources_filter_general)),
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(notificationChannel)
        }

        notificationManager.notify(
            VKRConstant.newNewsFromFavoriteSourcesNotificationId,
            getNotification()
        )
    }
}