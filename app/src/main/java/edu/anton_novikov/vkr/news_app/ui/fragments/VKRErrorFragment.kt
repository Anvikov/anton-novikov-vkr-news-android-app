package edu.anton_novikov.vkr.news_app.ui.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.ui.activities.VKRMainActivity
import edu.anton_novikov.vkr.news_app.ui.activities.VKRNewsFromFavoriteSourcesActivity

class VKRErrorFragment(
    private val mContext: Context,
    private val mFragmentManager: FragmentManager,
    private val mImageView: Int,
    private val mTitle: String,
    private val mMessage: String
) : Fragment() {

    private var mView: View = View(mContext)
    private lateinit var errorImage: ImageView
    private lateinit var errorTitle: TextView
    private lateinit var errorMessage: TextView
    private lateinit var buttonRetry: Button
    private lateinit var errorLayout: RelativeLayout


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.error_fragment, container, false)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findAllComponentsForError()
    }

    override fun onResume() {
        showErrorMessage(mImageView, mTitle, mMessage)
        super.onResume()
    }

    private fun showErrorMessage(imageView: Int, title: String, message: String) {
        errorImage.setImageResource(imageView)
        errorTitle.text = title
        errorMessage.text = message

        buttonRetry.setOnClickListener {
            when (requireActivity().javaClass) {

                VKRMainActivity::class.java -> {
                    mFragmentManager.beginTransaction()
                        .replace(
                            R.id.fragmentLayout,
                            VKRTopHeadlines(mContext, mFragmentManager)
                        )
                        .commit()

                    requireActivity().findViewById<BottomNavigationView>(R.id.bottom_navigation)
                        .menu.findItem(R.id.action_top_headlines).isChecked = true

                    hideKeyboard(mContext, requireView())
                }

                VKRNewsFromFavoriteSourcesActivity::class.java -> {
                    mFragmentManager.beginTransaction()
                        .replace(
                            R.id.fragmentLayout,
                            VKRNewsFromFavoriteSourcesFragment(mContext, mFragmentManager)
                        )
                        .commit()
                }
            }
        }

    }

    private fun findAllComponentsForError() {
        errorLayout = mView.findViewById(R.id.errorLayout)
        errorImage = mView.findViewById(R.id.errorImage)
        errorTitle = mView.findViewById(R.id.errorTitle)
        errorMessage = mView.findViewById(R.id.errorMessage)
        buttonRetry = mView.findViewById(R.id.buttonRetry)
    }

    private fun hideKeyboard(context: Context, view: View) {
        val inputMethodManager: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}