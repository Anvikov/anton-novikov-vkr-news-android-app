package edu.anton_novikov.vkr.news_app.services

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.core.content.ContextCompat.getSystemService
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.github.kittinunf.fuel.json.responseJson
import com.github.kittinunf.result.Result
import edu.anton_novikov.vkr.news_app.api.VKRAPIService
import edu.anton_novikov.vkr.news_app.base.VKRApplication
import edu.anton_novikov.vkr.news_app.enums.VKRNewsTypes
import edu.anton_novikov.vkr.news_app.utils.VKRDisplayUtils

class VKRNetworkService {
    companion object {

        fun loadHeadlines(
            listener: LoadHeadlinesListener
        ) {
            VKRAPIService.getHeadlines()
                .responseJson { _, _, result ->
                    if (result is Result.Success) {
                        VKRStorageService.saveNews(result.get().obj(), VKRNewsTypes.HEADLINES.type)
                        listener.onHeadlinesNewsLoadComplete()
                    } else {
                        listener.onHeadlinesNewsLoadFailed(result.component2()?.message.toString())
                    }
                }
        }

        fun loadSources(
            listener: LoadSourcesListener,
            category: String?,
            language: String?,
            country: String?
        ) {
            VKRAPIService.getSources(category, language, country).responseJson { _, _, result ->
                if (result is Result.Success) {
                    VKRStorageService.saveSources(result.get().obj())
                    listener.onSourcesLoadComplete()
                } else {
                    listener.onSourcesLoadFailed(result.component2()?.message.toString())
                }
            }
        }

        fun loadNews(
            listener: LoadNewsListener, query: String,
            sources: String, page: Int, type: String
        ) {
            VKRAPIService.getNews(query, sources, page).responseJson { _, _, result ->
                if (result is Result.Success) {
                    VKRStorageService.saveNews(result.get().obj(), type)
                    listener.onNewsLoadComplete()
                } else {
                    listener.onNewsLoadFailed(result.component2()?.message.toString())
                }
            }
        }

        fun loadImage(
            url: String,
            into: ImageView
        ) {
            Glide.with(VKRApplication.getContent())
                .load(url).centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(into)
        }

        fun loadImageAndHideProgressBar(
            url: String,
            into: ImageView, progressBar: ProgressBar
        ) {
            Glide.with(VKRApplication.getContent()).load(url)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE).fitCenter()
                .timeout(3000)
                .placeholder(VKRDisplayUtils.getRandomDrawableColor())
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressBar.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressBar.visibility = View.GONE
                        return false
                    }

                })
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(into)
        }

        fun isInternetAvailable(context: Context): Boolean {
            var result = false
            val connectivityManager = getSystemService(context, ConnectivityManager::class.java)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val networkCapabilities = connectivityManager?.activeNetwork ?: return false
                val actNw =
                    connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
                result = when {
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            } else {
                connectivityManager.run {
                    @Suppress("DEPRECATION")
                    connectivityManager?.activeNetworkInfo?.run {
                        result = when (type) {
                            ConnectivityManager.TYPE_WIFI -> true
                            ConnectivityManager.TYPE_MOBILE -> true
                            ConnectivityManager.TYPE_ETHERNET -> true
                            else -> false
                        }

                    }
                }
            }

            return result
        }
    }

    interface LoadHeadlinesListener {
        fun onHeadlinesNewsLoadComplete()
        fun onHeadlinesNewsLoadFailed(error: String)
    }

    interface LoadSourcesListener {
        fun onSourcesLoadComplete()
        fun onSourcesLoadFailed(error: String)
    }

    interface LoadNewsListener {
        fun onNewsLoadComplete()
        fun onNewsLoadFailed(error: String)
    }

}