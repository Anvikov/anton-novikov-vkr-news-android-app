package edu.anton_novikov.vkr.news_app.api

import android.webkit.WebView
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import edu.anton_novikov.vkr.news_app.base.VKRApplication
import edu.anton_novikov.vkr.news_app.base.VKRConstant
import edu.anton_novikov.vkr.news_app.utils.VKRLocaleUtils
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper


interface VKRAPIService {
    companion object {
        private val manager = FuelManager().apply {
            basePath = VKRConstant.ENDPOINT_API
            timeoutInMillisecond = 10000
            baseHeaders = mapOf("User-Agent" to getUserAgent())
        }

        private fun getUserAgent(): String {
            return WebView(VKRApplication.getContent()).settings.userAgentString
        }

        fun getHeadlines(): Request {
            val params = arrayListOf<Pair<String, Any?>>()
            params.add(Pair("country", VKRLocaleUtils.getCountry()))
            params.add(Pair("apiKey", VKRConstant.apiKey))

            return manager.get("/top-headlines", params)
        }

        fun getSources(category: String?, language: String?, country: String?): Request {
            val params = arrayListOf<Pair<String, Any?>>()
            if (category?.isNotBlank() == true) {
                params.add(Pair("category", category))
            }

            if (language?.isNotBlank() == true) {
                params.add(Pair("language", language))
            }

            if (country?.isNotBlank() == true) {
                params.add(Pair("country", country))
            }
            params.add(Pair("apiKey", VKRConstant.apiKey))

            return manager.get("/sources", params)
        }

        fun getNews(query: String, sources: String, page: Int): Request {
            val params = arrayListOf<Pair<String, Any?>>()
            if (query.isNotBlank()) {
                params.add(Pair("q", query))
            }

            if (sources.isNotBlank()) {
                params.add(Pair("sources", sources))
            }

            if (page > 1) {
                params.add(Pair("page", page))
            }

            params.add(Pair("pageSize", VKRPreferencesHelper.newsPerPage))

            params.add(Pair("apiKey", VKRConstant.apiKey))

            return manager.get("/everything", params)
        }
    }
}