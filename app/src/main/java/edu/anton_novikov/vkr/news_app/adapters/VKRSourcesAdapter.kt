package edu.anton_novikov.vkr.news_app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.model.view.VKRSourceView
import edu.anton_novikov.vkr.news_app.services.VKRStorageService
import edu.anton_novikov.vkr.news_app.ui.activities.VKRMainActivity
import edu.anton_novikov.vkr.news_app.ui.fragments.VKRFavoriteSourcesFragment
import edu.anton_novikov.vkr.news_app.utils.VKRDisplayUtils
import edu.anton_novikov.vkr.news_app.utils.VKRLocaleUtils
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper

class VKRSourcesAdapter(
    private var listItems: List<VKRSourceView>,
    private val mContext: Context
) :
    RecyclerView.Adapter<VKRSourcesAdapter.MyViewHolderForSources>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolderForSources {
        val view: View = LayoutInflater.from(mContext).inflate(R.layout.sources_item, parent, false)
        return MyViewHolderForSources(view)
    }

    override fun onBindViewHolder(holder: MyViewHolderForSources, position: Int) {
        val model = listItems[position]

        holder.name.text = model.name
        holder.description.text = model.description
        holder.url.text = model.url
        holder.country.text = VKRLocaleUtils.getCountryFromCode(model.country ?: "")
        holder.category.text = VKRLocaleUtils.getFormattedSourceCategory(model.category ?: "")
        holder.language.text = VKRLocaleUtils.getLanguageFromCode(model.language ?: "")

        if (model.isFavorite) {
            holder.favoriteIcon.setImageResource(R.drawable.ic_baseline_favorite_35)
        } else {
            holder.favoriteIcon.setImageResource(R.drawable.ic_baseline_favorite_border_35)
        }

        initListeners(holder, position, model)
        setBottomMargin(holder, position)
    }

    private fun initListeners(holder: MyViewHolderForSources, position: Int, model: VKRSourceView) {
        holder.itemView.setOnClickListener {
            when {
                model.isFavorite -> changeSourceType(position, model)
                VKRStorageService.loadFavoriteSources().size < 20 -> changeSourceType(
                    position,
                    model
                )
                else -> Toast.makeText(
                    mContext,
                    mContext.getString(R.string.maximum_favorites_sources),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        holder.url.setOnClickListener {
            if (model.url?.isNotBlank() == true) {
                VKRMainActivity.openUrl(model.url ?: "")
            }
        }
    }

    private fun changeSourceType(position: Int, model: VKRSourceView) {
        VKRStorageService.changeSourceIsFavorite(model.id)
        model.isFavorite = model.isFavorite.not()
        VKRPreferencesHelper.favoriteSourcesString =
            VKRFavoriteSourcesFragment.getFavoritesSources()

        notifyItemChanged(position)
    }

    private fun setBottomMargin(holder: MyViewHolderForSources, position: Int) {
        val itemLayoutParams = holder.itemView.layoutParams as ViewGroup.MarginLayoutParams
        val bottomMargin = if (position == itemCount - 1) 60 else 1
        itemLayoutParams.bottomMargin = VKRDisplayUtils.dpToPx(bottomMargin)
        holder.itemView.layoutParams = itemLayoutParams
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    fun setSources(data: List<VKRSourceView>) {
        listItems = data
        notifyDataSetChanged()
    }

    class MyViewHolderForSources(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        var name: TextView = itemView.findViewById(R.id.name);
        var description: TextView = itemView.findViewById(R.id.description);
        var url: TextView = itemView.findViewById(R.id.url)
        var country: TextView = itemView.findViewById(R.id.country)
        var category: TextView = itemView.findViewById(R.id.category)
        var favoriteIcon: ImageView = itemView.findViewById(R.id.favorites)
        var language: TextView = itemView.findViewById(R.id.language)
    }
}