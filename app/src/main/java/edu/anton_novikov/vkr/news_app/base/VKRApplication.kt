package edu.anton_novikov.vkr.news_app.base

import android.app.Application
import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration

class VKRApplication : Application() {
    init {
        instance = this
    }
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val config = RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded()
            .allowWritesOnUiThread(true)
            .build()
        Realm.setDefaultConfiguration(config)
    }

    companion object{
        private var instance: VKRApplication? = null

        fun getContent():Context{
            return instance!!.applicationContext
        }

        fun getApplication():VKRApplication?{
            return  instance
        }
    }
}