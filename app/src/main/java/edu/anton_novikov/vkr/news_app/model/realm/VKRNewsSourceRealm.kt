package edu.anton_novikov.vkr.news_app.model.realm;

import io.realm.RealmObject

open class VKRNewsSourceRealm(
    var id: String?,
    var name: String?,
) : RealmObject() {
    constructor() : this(
        "", ""
    )
}
