package edu.anton_novikov.vkr.news_app.enums

enum class VKRNewsTypes(val type: String) {
    HEADLINES("Headlines"),
    FROM_FAVORITES("From Favorites"),
    FROM_SEARCH("From Search")
}