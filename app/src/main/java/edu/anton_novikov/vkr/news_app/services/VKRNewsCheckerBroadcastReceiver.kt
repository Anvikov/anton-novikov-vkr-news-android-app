package edu.anton_novikov.vkr.news_app.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper

class VKRNewsCheckerBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == Intent.ACTION_BOOT_COMPLETED) {
            context?.let {
                if (VKRPreferencesHelper.favoriteSourcesNewsCheckerServiceEnable) {
                    it.startService(Intent(it, VKRFavoritesSourcesNewsChecker::class.java))
                }
            }
        }
    }
}