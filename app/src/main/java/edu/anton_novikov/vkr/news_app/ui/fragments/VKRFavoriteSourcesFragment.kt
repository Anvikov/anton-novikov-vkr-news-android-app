package edu.anton_novikov.vkr.news_app.ui.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.adapters.VKRSourcesAdapter
import edu.anton_novikov.vkr.news_app.model.view_model.VKRSourcesViewModel
import edu.anton_novikov.vkr.news_app.services.VKRStorageService
import edu.anton_novikov.vkr.news_app.ui.activities.VKRNewsFromFavoriteSourcesActivity

class VKRFavoriteSourcesFragment(
    private val mContext: Context,
    private val mFragmentManager: FragmentManager
) : Fragment() {

    private lateinit var mView: View
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var placeholder: TextView
    private lateinit var fab: ExtendedFloatingActionButton

    private lateinit var recyclerView: RecyclerView
    private var recyclerViewAdapter = VKRSourcesAdapter(emptyList(), mContext)

    private val favoriteSources: VKRSourcesViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.sources_favorite_fragment, container, false)

        initViews()
        initListeners()
        setHasOptionsMenu(true)
        getFavoritesSources()

        return mView
    }

    private fun initViews() {
        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh_layout)
        placeholder = mView.findViewById(R.id.no_favorites_sources_textview)
        recyclerView = mView.findViewById(R.id.recyclerView)
        fab = mView.findViewById(R.id.favoriteSourcesFAB)

        initRecyclerView()
    }

    private fun initRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(mContext)

        recyclerView.adapter = recyclerViewAdapter
        recyclerViewAdapter.notifyDataSetChanged()
    }

    private fun initListeners() {
        swipeRefreshLayout.setOnRefreshListener { getFavoritesSources() }

        fab.setOnClickListener {
            startActivity(Intent(mContext, VKRNewsFromFavoriteSourcesActivity::class.java))
        }
    }

    private fun getFavoritesSources() {
        favoriteSources.getFavoriteSources().observe(viewLifecycleOwner, {
            recyclerViewAdapter.setSources(it)
            showPlaceholder(it.isEmpty())
            swipeRefreshLayout.isRefreshing = false
        })
    }

    private fun showPlaceholder(isNeedShow: Boolean) {
        if (!isNeedShow) {
            recyclerView.visibility = View.VISIBLE
            fab.visibility = View.VISIBLE
            placeholder.visibility = View.GONE
        } else {
            recyclerView.visibility = View.GONE
            fab.visibility = View.GONE
            placeholder.visibility = View.VISIBLE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.favorite_sources_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_clear_all_favorite_sources -> {
                VKRStorageService.clearAllFavoriteSources()
                recyclerViewAdapter.notifyDataSetChanged()
                Toast.makeText(mContext, getString(R.string.all_filters_was_cleared), Toast.LENGTH_LONG).show()
            }
        }

        return true
    }

    companion object {
        fun getFavoritesSources(): String {
            val sources = VKRStorageService.loadFavoriteSources()
            var favoritesSources = ""

            for (item in sources) {
                favoritesSources += "${item.id},"
            }
            return if (favoritesSources.isNotBlank()) favoritesSources.substring(
                0,
                favoritesSources.length - 1
            ) else favoritesSources
        }
    }
}