package edu.anton_novikov.vkr.news_app.ui.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.NestedScrollView
import com.google.android.material.appbar.AppBarLayout
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.model.view.VKRNewsView
import edu.anton_novikov.vkr.news_app.model.view_model.VKRNewsViewModel
import edu.anton_novikov.vkr.news_app.services.VKRNetworkService
import edu.anton_novikov.vkr.news_app.utils.VKRDateUtils
import edu.anton_novikov.vkr.news_app.utils.VKRPreferencesHelper
import kotlin.math.abs

class VKRNewsDetailActivity : AppCompatActivity(), AppBarLayout.OnOffsetChangedListener {
    private lateinit var newsImage: ImageView
    private lateinit var appbarTitle: TextView
    private lateinit var appbarSubtitle: TextView
    private lateinit var date: TextView
    private lateinit var time: TextView
    private lateinit var title: TextView
    private lateinit var webView: WebView

    private var isHideToolBarView = false
    private lateinit var dateBehavior: ConstraintLayout
    private lateinit var titleAppbar: LinearLayout
    private var backButton: ConstraintLayout? = null
    private var shareButton: ConstraintLayout? = null
    private var openInBrowser: ConstraintLayout? = null
    private var topButtonsBackground: ImageView? = null
    private var newsDetailsCard: ConstraintLayout? = null
    private var nestedScrollView: NestedScrollView? = null

    private val newsViewModel: VKRNewsViewModel by viewModels()
    private var news: VKRNewsView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.news_detail_activity)

        initViews()
        initListeners()

        newsViewModel.getSingleNews(intent.getStringExtra(getString(R.string.url)) ?: "")
            .observe(this, {
                news = it
                fillNewsDetail(it)
            })
        showTopButtonsBackgroundAndTitle(isNeedShow = false, isInit = true)
    }

    private fun initViews() {
        backButton = findViewById(R.id.newsDetailsBackButtonLayout)
        shareButton = findViewById(R.id.newsDetailsShareButtonLayout)
        openInBrowser = findViewById(R.id.newsDetailsOpenInBrowserButtonLayout)
        topButtonsBackground = findViewById(R.id.newsDetailsTopButtonsLayoutBackground)
        nestedScrollView = findViewById(R.id.nestedScrollView)

        newsDetailsCard = findViewById(R.id.newsDetailsCard)
        dateBehavior = findViewById(R.id.date_behavior)
        titleAppbar = findViewById(R.id.title_appbar)
        newsImage = findViewById(R.id.newsDetailsImage)
        appbarTitle = findViewById(R.id.title_on_appbar)
        appbarSubtitle = findViewById(R.id.subtitle_on_appbar)
        date = findViewById(R.id.date)
        time = findViewById(R.id.time)
        title = findViewById(R.id.name)
        webView = findViewById(R.id.webView)
    }

    private fun initListeners() {
        backButton?.setOnClickListener { finish() }

        shareButton?.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_SEND)
                val body = news?.title + "\n" + news?.url + "\n"

                intent.type = "text/plan"
                intent.putExtra(Intent.EXTRA_SUBJECT, news?.source)
                intent.putExtra(Intent.EXTRA_TEXT, body)

                startActivity(Intent.createChooser(intent, getString(R.string.share_with)))
            } catch (e: Exception) {
                Toast.makeText(
                    baseContext,
                    getString(R.string.cannot_be_shared),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        openInBrowser?.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(news?.url)
            startActivity(intent)
        }


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            nestedScrollView?.setOnScrollChangeListener { _, _, _, _, _ ->
                showTopButtonsBackgroundAndTitle(checkIsNewsDetailsCardVisible().not(), false)
            }
        } else {
            showTopButtonsBackgroundAndTitle(isNeedShow = true, isInit = false)
        }
    }

    private fun checkIsNewsDetailsCardVisible(): Boolean {
        val viewRect = Rect()
        newsDetailsCard?.getGlobalVisibleRect(viewRect)

        val screenRect = Rect(
            0,
            0,
            resources.displayMetrics.widthPixels,
            resources.displayMetrics.heightPixels
        )

        return screenRect.intersect(viewRect)
    }

    private fun showTopButtonsBackgroundAndTitle(isNeedShow: Boolean, isInit: Boolean) {
        val alpha = if (isNeedShow) 1f else 0f
        val duration = if (!isInit) 300L else 0L

        topButtonsBackground?.animate()?.alpha(alpha)?.duration = duration
        appbarTitle.animate()?.alpha(alpha)?.duration = duration
        appbarSubtitle.animate()?.alpha(alpha)?.duration = duration
    }

    private fun fillNewsDetail(news: VKRNewsView) {
        val source = news.source ?: getString(R.string.unknown)
        val author = if (news.author?.isNotBlank() == true) "• ${news.author}" else ""

        appbarTitle.text = source
        appbarSubtitle.text = news.url
        date.text = VKRDateUtils.formatDate(news.publishedAt)
        title.text = news.title

        time.text = getString(
            R.string.time,
            source,
            author,
            "• ${VKRDateUtils.convertDateToTimeFormat(news.publishedAt)}"
        )

        initWebViewAndLoadNews()
        VKRNetworkService.loadImage(news.urlToImage ?: "", newsImage)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebViewAndLoadNews() {
        webView.settings.loadsImagesAutomatically = true
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.settings.setSupportZoom(true)
        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webView.webViewClient = object : WebViewClient() {}

        webView.settings.cacheMode =
            if (VKRPreferencesHelper.newsCacheEnable) WebSettings.LOAD_CACHE_ELSE_NETWORK else WebSettings.LOAD_NO_CACHE

        webView.loadUrl(news?.url ?: "")
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        val maxScroll = appBarLayout!!.totalScrollRange
        val percentage = (abs(verticalOffset) / maxScroll).toFloat()

        if (percentage == 1f && isHideToolBarView) {
            dateBehavior.visibility = View.GONE
            titleAppbar.visibility = View.VISIBLE
            isHideToolBarView = !isHideToolBarView
        } else if (percentage < 1f && !isHideToolBarView) {
            dateBehavior.visibility = View.VISIBLE
            titleAppbar.visibility = View.GONE
            isHideToolBarView = !isHideToolBarView
        }
    }
}