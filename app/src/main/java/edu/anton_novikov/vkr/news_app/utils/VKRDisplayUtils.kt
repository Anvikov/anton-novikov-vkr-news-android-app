package edu.anton_novikov.vkr.news_app.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentManager
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.base.VKRApplication
import edu.anton_novikov.vkr.news_app.ui.fragments.VKRErrorFragment
import kotlin.math.roundToInt
import kotlin.random.Random

class VKRDisplayUtils {
    companion object {
        fun getRandomDrawableColor(): ColorDrawable {
            val colors =
                VKRApplication.getContent().resources.getStringArray(R.array.images_placeholder_colors)

            return ColorDrawable(Color.parseColor(colors[Random.nextInt(colors.size)]))
        }

        fun showNetworkError(context: Context, fragmentManager: FragmentManager, error: String) {
            showErrorFragment(
                context,
                fragmentManager,
                context.getString(R.string.oops),
                "${context.getString(R.string.network_error_title)}\n$error"
            )
        }

        fun showError(context: Context, fragmentManager: FragmentManager, error: String) {
            showErrorFragment(
                context,
                fragmentManager,
                context.getString(R.string.oops),
                "Error, Please Try Again\n$error"
            )
        }

        private fun showErrorFragment(
            context: Context,
            fragmentManager: FragmentManager,
            title: String,
            error: String
        ) {
            fragmentManager.beginTransaction().replace(
                R.id.fragmentLayout, VKRErrorFragment(
                    context, fragmentManager,
                    R.drawable.oops,
                    title,
                    error
                )
            ).commit()
        }

        fun dpToPx(dp: Int): Int {
            return (dp * (VKRApplication.getContent().resources.displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
        }

        fun hideKeyboard(view: View) {
            val inputMethodManager: InputMethodManager =
                VKRApplication.getContent()
                    .getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun capitalize(word: String): String {
            return "${word.substring(0, 1).toUpperCase(VKRLocaleUtils.getLocale())}${
                word.substring(
                    1
                )
            }"
        }
    }
}