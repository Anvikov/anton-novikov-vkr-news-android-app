package edu.anton_novikov.vkr.news_app.utils

import android.widget.Toast
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.base.VKRApplication
import kotlin.math.ceil
import kotlin.math.min

class VKRPagination {
    var page = 1

    fun nextPage(maxItemsCount: Int) {
        val context = VKRApplication.getContent()
        if (page < maxItemsCount / VKRPreferencesHelper.newsPerPage) {
            page++
        } else {
            Toast.makeText(
                context, context.getString(
                    R.string.max_page
                ), Toast.LENGTH_SHORT
            ).show()
        }
    }

    fun previousPage() {
        if (page > 1) {
            page--
        }
    }

    fun resetPage() {
        page = 1
    }

    fun lastPage(newsCount: Int) {
        page = ceil((newsCount / VKRPreferencesHelper.newsPerPage).toDouble()).toInt()
    }

    fun getNewsListIndex(newsCount: Int): List<Int> {
        val newsPerPage = VKRPreferencesHelper.newsPerPage
        val start = (page * newsPerPage) - newsPerPage
        val end = min(start + newsPerPage, newsCount)

        return listOf(start, end)
    }
}