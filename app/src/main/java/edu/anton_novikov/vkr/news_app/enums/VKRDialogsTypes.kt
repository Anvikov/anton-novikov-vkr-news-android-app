package edu.anton_novikov.vkr.news_app.enums

enum class VKRDialogsTypes(val type: String) {
    CATEGORY("category"),
    LANGUAGE("language"),
    COUNTRY("country")
}