package edu.anton_novikov.vkr.news_app.utils

import android.os.Build
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.base.VKRApplication
import java.util.*

class VKRLocaleUtils {
    companion object {
        fun getLocale(): Locale {
            val context = VKRApplication.getContent()

            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                context.resources.configuration.locales.get(0)
            } else {
                @Suppress("DEPRECATION")
                context.resources.configuration.locale
            }
        }

        fun getCountry(): String {
            return getLocale().country
        }

        fun getLanguageFromCode(code: String): String {
            return VKRDisplayUtils.capitalize(Locale(code).displayLanguage)
        }

        fun getFormattedSourceCategory(category: String): String {
            val initCategories =
                VKRApplication.getContent().resources.getStringArray(R.array.init_sources_fragment_filter_category_list)
            val categories =
                VKRApplication.getContent().resources.getStringArray(R.array.sources_fragment_filter_category_list)

            return VKRDisplayUtils.capitalize(categories[initCategories.indexOf(category)])
        }

        fun getCountryFromCode(code: String): String {
            return VKRDisplayUtils.capitalize(Locale("", code).displayCountry)
        }

        fun getLanguagesFromCodes(codes: Array<String>): List<CharSequence> {
            return codes.map { code -> getLanguageFromCode(code) }
        }

        fun getCountriesFromCodes(codes: Array<String>): List<CharSequence> {
            return codes.map { code -> getCountryFromCode(code) }
        }

        fun getFormattedSourceCategories(categories: Array<String>): List<CharSequence> {
            return categories.map { category -> getFormattedSourceCategory(category) }
        }
    }
}