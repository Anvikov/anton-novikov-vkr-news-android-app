package edu.anton_novikov.vkr.news_app.ui.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import edu.anton_novikov.vkr.news_app.R
import edu.anton_novikov.vkr.news_app.ui.fragments.VKRNewsFromFavoriteSourcesFragment

class VKRNewsFromFavoriteSourcesActivity : AppCompatActivity(){


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.news_from_favorite_sources_activity)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title= getString(R.string.news_from_favorites_sources)
        supportFragmentManager.beginTransaction().replace(
            R.id.fragmentLayout,
            VKRNewsFromFavoriteSourcesFragment(this, supportFragmentManager)
        ).commit()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }

        return true
    }


}